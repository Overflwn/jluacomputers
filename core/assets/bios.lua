--[[
		LuaComputers BIOS file.
		
	NOTE: This is very experimental.
]]
local _ver = 0.3

print("BIOS Ver.: "..tostring(_ver))

local fixedEnv = {}
for each, thing in pairs(_G) do
	if each ~= "io" then
		fixedEnv[each] = thing
	end
end
fixedEnv["_system"] = nil
print(tostring(fixedEnv._filesystem))
fixedEnv.io = {}

fixedEnv.io.open = function(path, mode)
	path = _filesystem.getRoot()..path
	return io.open(path, mode)
end

fixedEnv.io.input = function(file)
	file = _filesystem.getRoot()..file
	return io.input(file)
end

fixedEnv.io.lines = function(filename)
	filename = _filesystem.getRoot()..filename
	return io.lines(filename)
end

fixedEnv.io.output = function(file)
	file = _filesystem.getRoot()..file
	return io.output(file)
end

function _G.loadfile(path, env)
	env = env or fixedEnv
	local file, err = _filesystem.open(path, "r")
	if not file then
		return false, err
	end
	local data = file:read("*a")
	file:close()
	return load(data, path, "t", env)
end

fixedEnv.loadfile = _G.loadfile

fixedEnv.system = {}

function fixedEnv.system.pullEvent(filter)
	return coroutine.yield(filter)
end

function fixedEnv.system.pushEvent(name, tArgs)
	--NOTE: tArgs is a table that contains 4 values MAXIMUM
	_system.pushEvent(name, tArgs)
end

function fixedEnv.system.setTimer(amount)
	return _system.setTimer(amount)
end

--Re-implement require, so it doesn't allow to load java classes
local oldRequire = require
local loaded = {}
local searchPaths = {
	"/",
	"/lib/"
}
function fixedEnv.require(sName)
	sName = tostring(sName)
	
	if not loaded[sName] then
		local tExistedBefore = {}
		for each, thing in pairs(fixedEnv) do
			tExistedBefore[each] = true
		end
		
		local bFound = false
		local sPath = nil
		for each, path in ipairs(searchPaths) do
			local list = _filesystem.list(path)
			for _, file in ipairs(list) do
				if file == sName or file == sName..".lua" then
					bFound = true
					sPath = path..file
					break
				end
			end
			if bFound then break end
		end
		if not bFound and _filesystem.exists(sName) then
			bFound = true
			sPath = sName
		end
		if bFound then
			local func_lib, err = loadfile(sPath, fixedEnv)
			if not func_lib then
				return false, err 
			end
			local ok, err = pcall(func_lib)
			if not ok then return false, err end
			loaded[sName] = {}
			for each, thing in pairs(fixedEnv) do
				if not tExistedBefore[each] then
					loaded[sName][each] = thing
				end
			end
			return loaded[sName]
		else
			return false, "library not found"
		end
	else
		return loaded[sName]
	end
end

function fixedEnv.addSearchPath(sPath)
	sPath = tostring(sPath)
	local bFound = false
	for each, path in ipairs(searchPaths) do
		if path == sPath then
			bFound = true
			break
		end
	end
	if not bFound then
		table.insert(searchPaths, sPath)
	end
end

fixedEnv["_G"] = fixedEnv
fixedEnv["_ENV"] = fixedEnv
fixedEnv["ennv"] = "fixedEnv"
setmetatable(fixedEnv, {__index=fixedEnv})



print("Trying to load init.lua...")
local init_func, err = loadfile("init.lua", fixedEnv)

if not init_func then return print("BIOS ERROR: Could not load init.lua. Are you sure the file exists? "..tostring(err)) end


--Run init.lua; NOTE: bios.lua does NOT create a loop as you can see, which means that once init.lua finishes completely,
--						the emulator is basically shut down too. (Will fix that later on though)
print("Running init.lua...")
init_func()
