/********************************************
* Copyright 2017 Patrick "Piorjade" Swierzy *
*********************************************/

package piorjade.luacomputers;

public class Debugger 
{
	public static boolean debug = false;
	public static void log(String message)
	{
		if(debug)
		{
			System.out.println(message);
		}
	}
}
