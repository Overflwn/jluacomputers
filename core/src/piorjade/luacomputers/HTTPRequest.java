package piorjade.luacomputers;

public class HTTPRequest 
{
	private String type;
	private String urlLink;
	private static long last_id = 0;
	private long id;
	
	/**
	 * Create a container for a HTTP request. (This gets processed in the HTTPAdapter thread.)
	 * @param type
	 * @param urlLink
	 */
	public HTTPRequest(String type, String urlLink)
	{
		this.type = type;
		this.urlLink = urlLink;
		this.id = last_id+1;
		last_id++;
	}
	
	/**
	 * Return the type of the request. (e.g. POST, GET, etc)
	 * @return
	 */
	public String getType()
	{
		return type;
	}
	
	/**
	 * Return the complete request URL.
	 * @return
	 */
	public String getUrlLink()
	{
		return urlLink;
	}
	
	/**
	 * Return the id of the request.
	 * @return
	 */
	public long getID()
	{
		return id;
	}
}
