/********************************************
* Copyright 2017 Patrick "Piorjade" Swierzy *
*********************************************/

package piorjade.luacomputers;

public class Timer 
{
	private float timeout;
	private float current;
	private long id;
	private static long lastID = 0;
	
	/**
	 * Create a timer.
	 * @param timeout
	 */
	public Timer(float timeout)
	{
		this.timeout = timeout;
		this.current = 0;
		this.id = lastID+1;
		lastID = lastID+1;
	}
	
	/**
	 * Add the passed amount to the internal counter.
	 * @param amount
	 * @return
	 */
	public boolean tick(float amount)
	{
		this.current += amount;
		if(this.current >= this.timeout)
		{
			return true;
		} else
		{
			return false;
		}
	}
	
	/**
	 * Return the ID of the timer.
	 * @return
	 */
	public long getID()
	{
		return id;
	}
}
