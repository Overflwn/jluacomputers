/********************************************
* Copyright 2017 Patrick "Piorjade" Swierzy *
*********************************************/

package piorjade.luacomputers;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import piorjade.config.Settings;
import piorjade.luacomputers.computer.Terminal;
import piorjade.luacomputers.computer.CAudioDevice;
import piorjade.luacomputers.computer.Emulator;
import piorjade.luacomputers.computer.EmulatorTask;
import piorjade.luacomputers.computer.HTTPAdapter;
import piorjade.luacomputers.computer.NetworkAdapter;
import piorjade.luacomputers.computer.NetworkAdapterSender;

/*
 * TODO:
 *  - Write basic OS
 *  - Clean code
 *
 *
 *
 * NOTES:
 *  - Good sources: LuaJ docs (duh), "Java multithreading synchronization",
 *    https://systembash.com/a-simple-java-udp-server-and-udp-client/,
 *    https://stackoverflow.com/questions/13582395/sharing-a-variable-between-multiple-different-threads,
 *
 */


public class StartClass extends ApplicationAdapter {
	private ShapeRenderer shapeRenderer;
	private Terminal computer_terminal;
	private Emulator computer_emulator;
	private EmulatorTask emu_task;
	private NetworkAdapter network_a;
	private HTTPAdapter http_a;
	private NetworkAdapterSender network_b;
	private CAudioDevice audio_device;
	private EventListener eventListener;

	//Things that need to be synchronized
	public ConcurrentLinkedQueue<NetworkMessage> messageQueue;
	public ConcurrentLinkedQueue<HTTPRequest> requestQueue;
	public ConcurrentLinkedQueue<Varargs> events;
	public volatile boolean adapterEnabled = true;
	public volatile boolean httpEnabled = true;
	public ConcurrentLinkedQueue<Timer> timers;

	//Window related
	public int window_width, window_height;
	public int terminal_width, terminal_height;

	private float fCounter = 0.f;
	private int fpsCount = 0;

	/**
	 * This class basically sets up the whole system.
	 * @param window_width
	 * @param window_height
	 */
	public StartClass(int window_width, int window_height)
	{
		//Set up everything intern
		this.window_width = window_width;
		this.window_height = window_height;
		this.terminal_width = Settings.RES_X;
		this.terminal_height = Settings.RES_Y;

		this.messageQueue = new ConcurrentLinkedQueue<NetworkMessage>();
		this.requestQueue = new ConcurrentLinkedQueue<HTTPRequest>();
		
		this.events = new ConcurrentLinkedQueue<Varargs>();
		this.timers = new ConcurrentLinkedQueue<Timer>();

		Debugger.log("DEBUG: Starting system..");

	}

	@Override
	public void create () {
		//The shapeRenderer is used to draw every single pixel, as it is able to draw rectangles directly, which we need in this case.
		shapeRenderer = new ShapeRenderer();

		//The eventListener takes over control of input events and "converts" that information to LuaComputer events and puts them into the queue
		eventListener = new EventListener(this);
		Gdx.input.setInputProcessor(eventListener);

		//Create a terminal and audio device
		computer_terminal = new Terminal(terminal_width, terminal_height, window_width, window_height);
		computer_terminal.setRenderer(shapeRenderer);
		audio_device = new CAudioDevice();
		//Pass the terminal, the StartClass instance and the audio device to the emulator
		//TODO: The terminal reference is currently completely useless in the emulator class, consider deleting that
		computer_emulator = new Emulator(computer_terminal, this, audio_device);

		//Try to create the network adapters
		//TODO: If they fail, they currently shut the whole program down; Feel free to suggest another way how we could handle this.
		try {
			network_a = new NetworkAdapter(this, Settings.NETWORK_PORT, Settings.RECEIVE_BUFFER_SIZE, "mainAdapter");
			network_a.start();
		} catch (Exception e) {
			System.err.println("Error creating network adapter (receiver).");
			e.printStackTrace();
			System.exit(1);
		}
		try {
			network_b = new NetworkAdapterSender(this, Settings.NETWORK_PORT, Settings.SEND_BUFFER_SIZE, "secondAdapter");
			network_b.start();
		} catch (Exception e) {
			System.err.println("Error creating network adapter (sender).");
			e.printStackTrace();
			System.exit(1);
		}
		http_a = new HTTPAdapter(this, "thirdAdapter");
		http_a.start();

		//Create the emulator thread and pass the emulator and the StartClass instance
		emu_task = new EmulatorTask(computer_emulator, "emu_task", this);
		emu_task.start();

	}

	@Override
	public void render () {
		// This loop handles the timers and drawing the terminal
		// TODO: Currently, the timers are updated here and it should work (because we use delta time), but it would be a bit better to update them in an extra thread I think

		//Clear window, start shapeRenderer
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		shapeRenderer.begin(ShapeType.Filled);

		//Get deltatime (time between last and this frame)
		float delta = Gdx.graphics.getDeltaTime();

		//Iterate through every existing timer to check if they are done and if they are, add an event and remove them from the timers list
		for(int i = 0; i < timers.size(); i++)
		{
			Timer timer = timers.poll();
			boolean finished = timer.tick(delta);
			if(finished)
			{
				LuaValue[] values = {LuaValue.valueOf("timer"), LuaValue.valueOf(timer.getID())};
				Varargs event = LuaValue.varargsOf(values);
				events.add(event);
			}else
			{
				//Re-insert it into the queue
				timers.add(timer);
			}
		}

		// TODO: Here the emulator used to get resumed, however, it was slow because the logic was basically framecapped which meant that if you had many events in the queue it would iterate slowly through them

		//Redraw the whole terminal
		//TODO: Currently everything works smoothly, however, my FPS with an 8-core processor on 4GHz and a GTX 960 are "only" 180,
		//		this might be bad. Consider remaking the array of pixels inside the Terminal class to only contain an one-dimensional array for the pixels (just y, x should be in one string containing information about the colors)
		//		Example:
		//			Let's say the width is 10
		//			y = 3
		//			pixels[y] = "fff0ffffff"
		//			This makes every pixel on the 4th line white except the pixel on 4|4 (base 1), which is black
		//			This would work because we only allow 16 colors for the gpu so we could assign every color a hexadecimal char.
		computer_terminal.drawTerminal();

		//Calculate FPS and display it on the window titlebar.
		shapeRenderer.end();
		fCounter += delta;
		fpsCount++;
		if(fCounter >= 1.f)
		{
			String enabledDisabled = "disabled";
			String httpEnabledDisabled = "disabled";
			if(adapterEnabled)
				enabledDisabled = "enabled";
			if(httpEnabled)
				httpEnabledDisabled = "enabled";
			Gdx.graphics.setTitle("LuaComputers | FPS: " + String.valueOf(fpsCount) + " | Networking: " + enabledDisabled + " | HTTP: " + httpEnabledDisabled);
			fpsCount = 0;
			fCounter = 0.f;
		}
	}

	@Override
	public void dispose () {
		//Get rid of everything libGDX related.
		shapeRenderer.dispose();
		audio_device.dispose();
	}
}
