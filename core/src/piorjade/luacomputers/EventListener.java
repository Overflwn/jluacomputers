/********************************************
* Copyright 2017 Patrick "Piorjade" Swierzy *
*********************************************/

package piorjade.luacomputers;

import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;

public class EventListener implements InputProcessor
{
	private StartClass mainClass;
	
	private int oldX, oldY;
	
	
	public EventListener(StartClass main)
	{
		mainClass = main;
	}
	
	@Override
	public boolean keyDown(int keycode) {
			LuaValue[] values = {LuaValue.valueOf("key_down"), LuaValue.valueOf(Input.Keys.toString(keycode))};
			Varargs event = LuaValue.varargsOf(values);
			mainClass.events.add(event);
			return false;
		
	}

	@Override
	public boolean keyUp(int keycode) {
			LuaValue[] values = {LuaValue.valueOf("key_up"), LuaValue.valueOf(Input.Keys.toString(keycode))};
			Varargs event = LuaValue.varargsOf(values);
			mainClass.events.add(event);
			return false;
	}

	@Override
	public boolean keyTyped(char character) {
			//TODO: Non-character keys (such as shift) return the character -1, but so does SPACE, which makes NO sense
			//		How tf am I supposed to fix this?
			LuaValue[] values = {LuaValue.valueOf("char"), LuaValue.valueOf(Character.toString(character))};
			Varargs event = LuaValue.varargsOf(values);
			mainClass.events.add(event);
			return false;
		
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
			int newX, newY;
			newX = Math.round(screenX / (mainClass.window_width/mainClass.terminal_width)) + 1;
			newY = Math.round(screenY / (mainClass.window_height/mainClass.terminal_height)) + 1;
			oldX = newX;
			oldY = newY;
			LuaValue[] values = {LuaValue.valueOf("mouse_down"), LuaValue.valueOf(button), LuaValue.valueOf(newX), LuaValue.valueOf(newY)};
			Varargs event = LuaValue.varargsOf(values);
			mainClass.events.add(event);
			return false;
		
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
			int newX, newY;
			newX = Math.round(screenX / (mainClass.window_width/mainClass.terminal_width)) + 1;
			newY = Math.round(screenY / (mainClass.window_height/mainClass.terminal_height)) + 1;
			oldX = 0;
			oldY = 0;
			LuaValue[] values = {LuaValue.valueOf("mouse_up"), LuaValue.valueOf(button), LuaValue.valueOf(newX), LuaValue.valueOf(newY)};
			Varargs event = LuaValue.varargsOf(values);
			mainClass.events.add(event);
			return false;
		
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
			int newX, newY;
			newX = Math.round(screenX / (mainClass.window_width/mainClass.terminal_width)) + 1;
			newY = Math.round(screenY / (mainClass.window_height/mainClass.terminal_height)) + 1;
			if (newX != oldX || newY != oldY)
			{
				LuaValue[] values = {LuaValue.valueOf("mouse_drag"), LuaValue.valueOf(newX), LuaValue.valueOf(newY)};
				Varargs event = LuaValue.varargsOf(values);
				mainClass.events.add(event);
				oldX = newX;
				oldY = newY;
			}
			return false;
		
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
			int newX, newY;
			newX = Math.round(screenX / (mainClass.window_width/mainClass.terminal_width)) + 1;
			newY = Math.round(screenY / (mainClass.window_height/mainClass.terminal_height)) + 1;
			LuaValue[] values = {LuaValue.valueOf("mouse_move"), LuaValue.valueOf(newX), LuaValue.valueOf(newY)};
			Varargs event = LuaValue.varargsOf(values);
			mainClass.events.add(event);
		
		return false;
	}
	
	@Override
	public boolean scrolled(int amount) {
			LuaValue[] values = {LuaValue.valueOf("mouse_scroll"), LuaValue.valueOf(amount)};
			Varargs event = LuaValue.varargsOf(values);
			mainClass.events.add(event);
			return false;
		
	}
	
}