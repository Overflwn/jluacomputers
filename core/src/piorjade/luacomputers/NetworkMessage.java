/********************************************
* Copyright 2017 Patrick "Piorjade" Swierzy *
*********************************************/

package piorjade.luacomputers;

public class NetworkMessage 
{
	private String targetAddress;
	private String message;
	
	/**
	 * Create a container for a network message. (This gets processed in the NetworkAdapterSender thread.)
	 * @param address
	 * @param message
	 */
	public NetworkMessage(String address, String message)
	{
		targetAddress = address;
		this.message = message;
	}
	
	/**
	 * Get the IP address of the target.
	 * @return
	 */
	public String getAddress()
	{
		return targetAddress;
	}
	
	/**
	 * Get the actual message.
	 * @return
	 */
	public String getMessage()
	{
		return message;
	}
}
