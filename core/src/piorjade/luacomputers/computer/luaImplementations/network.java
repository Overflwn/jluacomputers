/********************************************
* Copyright 2017 Patrick "Piorjade" Swierzy *
*********************************************/

package piorjade.luacomputers.computer.luaImplementations;

import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.ZeroArgFunction;

import piorjade.luacomputers.NetworkMessage;
import piorjade.luacomputers.StartClass;

//TODO: Documentation; Cleanup
public class network 
{
	static StartClass mainClass;
	
	public static void setMainClass(StartClass newMain)
	{
		mainClass = newMain;
	}
	
	public static class toggleAdapter extends ZeroArgFunction
	{
		@Override
		public LuaValue call() {
			mainClass.adapterEnabled = !mainClass.adapterEnabled;
			return LuaValue.NIL;
		}
	}
	
	public static class enabled extends ZeroArgFunction
	{
		@Override
		public LuaValue call() {
			return LuaValue.valueOf(mainClass.adapterEnabled);
		}
	}
	
	public static class send extends TwoArgFunction
	{
		@Override
		public LuaValue call(LuaValue arg0, LuaValue arg1) {
			NetworkMessage msg = new NetworkMessage(arg0.toString(), arg1.toString());
			synchronized(mainClass.messageQueue)
			{
				mainClass.messageQueue.add(msg);
			}
			return LuaValue.NIL;
		}
	}
	
}
