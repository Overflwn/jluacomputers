/********************************************
* Copyright 2017 Patrick "Piorjade" Swierzy *
*********************************************/

package piorjade.luacomputers.computer.luaImplementations;

import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.VarArgFunction;

import piorjade.luacomputers.computer.CAudioDevice;

/**
 * Wrapper for the audio device.
 * @author patrick
 *
 */
public class audio 
{
	static CAudioDevice theAudioDevice;
	
	public static void setAudioDevice(CAudioDevice audioDevice)
	{
		theAudioDevice = audioDevice;
	}
	
	public static CAudioDevice getAudioDevice()
	{
		return theAudioDevice;
	}
	
	public static class playSound extends VarArgFunction
	{
		@Override
		public Varargs invoke(Varargs args)
		{
			boolean success = false;
			
			if(args.narg() == 4)
			{
				System.out.println("Pan: " + args.arg(3).tofloat());
				success = theAudioDevice.playSound(args.arg(1).toString(), args.arg(2).tofloat(), args.arg(3).tofloat(), args.arg(4).tolong());
			}else if(args.narg() == 3)
			{
				success = theAudioDevice.playSound(args.arg(1).toString(), args.arg(2).tofloat(), args.arg(3).tofloat());
			}else
			{
				success = theAudioDevice.playSound(args.arg1().toString(), 1.0f, 0f);
			}
			if(success)
				return LuaValue.TRUE;
			else
			{
				LuaValue[] returns = {LuaValue.FALSE, LuaValue.valueOf("Sound not loaded.")};
				return LuaValue.varargsOf(returns);
			}
			
		}
	}
	
	public static class loadSound extends TwoArgFunction
	{

		@Override
		public LuaValue call(LuaValue arg0, LuaValue arg1) {
			boolean success = theAudioDevice.loadSound(arg0.toString(), arg1.toString());
			if(success)
				return LuaValue.TRUE;
			else
				return LuaValue.FALSE;
		}
		
	}
}
