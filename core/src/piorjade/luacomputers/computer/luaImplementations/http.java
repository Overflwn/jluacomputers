package piorjade.luacomputers.computer.luaImplementations;

import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.ZeroArgFunction;

import piorjade.luacomputers.HTTPRequest;
import piorjade.luacomputers.StartClass;

public class http 
{
	static StartClass mainClass;
	
	public static void setMainClass(StartClass newMain)
	{
		mainClass = newMain;
	}
	
	public static class toggleAdapter extends ZeroArgFunction
	{
		@Override
		public LuaValue call() 
		{
			mainClass.httpEnabled = !mainClass.httpEnabled;
			return LuaValue.NIL;
		}
	}
	
	public static class enabled extends ZeroArgFunction
	{

		@Override
		public LuaValue call() {
			// TODO Auto-generated method stub
			return LuaValue.valueOf(mainClass.httpEnabled);
		}
		
	}
	
	public static class send extends TwoArgFunction
	{

		@Override
		public LuaValue call(LuaValue arg0, LuaValue arg1) {
			HTTPRequest request = new HTTPRequest(arg0.toString(), arg1.toString());
			synchronized(mainClass.requestQueue)
			{
				mainClass.requestQueue.add(request);
			}
			return LuaValue.valueOf(request.getID());
		}
		
	}
}
