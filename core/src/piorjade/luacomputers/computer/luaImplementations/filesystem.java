/********************************************
* Copyright 2017 Patrick "Piorjade" Swierzy *
*********************************************/

package piorjade.luacomputers.computer.luaImplementations;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.ZeroArgFunction;

import piorjade.luacomputers.Debugger;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 * TODO: This is deprecated.
 * @author patrick
 *
 */
class CFile
{
	String path;
	String fileData;
	char mode;
	int pos = 0;
	boolean closed = false;
	public CFile(String path, String fileData, char mode)
	{
		this.path = path;
		this.fileData = fileData;
		this.mode = mode;
	}
	
	public LuaValue read(int length)
	{
		if (!closed && mode == 'r')
		{
			int actualLength = length;
			if (pos+length > fileData.length())
			{
				actualLength = fileData.length()-pos;
			}
			if (actualLength < 1)
			{
				return LuaValue.NIL;
			}
			String sub = fileData.substring(pos, pos+actualLength);
			return LuaValue.valueOf(sub);
		} else
		{
			return LuaValue.NIL;
		}
		
	}
	
	public LuaValue readAll()
	{
		if (!closed && mode == 'r')
			return LuaValue.valueOf(fileData);
		else
			return LuaValue.NIL;
	}
	
	public void write(String data)
	{
		if(!closed && mode=='w')
		{
			String newData = data.toString();
			fileData = fileData+newData;
		}
	}
	
	public void writeLine(LuaValue data)
	{
		if(!closed && mode == 'w')
		{
			String newData = data.toString();
			fileData = fileData+newData+"\n";
		}
	}
	
	public LuaValue close()
	{
		if(!closed)
		{
			closed = true;
			if(mode == 'w')
			{
				List<String> lines = Arrays.asList(fileData);
				Path file = Paths.get(path);
				try {
					Files.write(file, lines, Charset.forName("UTF-8"));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					System.err.format("Error while writing to file '%s'.", path);
					e.printStackTrace();
					return LuaValue.valueOf(e.getMessage());
				}
			}
		}
		return LuaValue.NIL;
			
	}
}

/**
 * Wrapper for the filesystem.
 * @author patrick
 *
 */
public class filesystem 
{
	static Globals lua_globals;
	
	public static void setGlobals(Globals globals)
	{
		lua_globals = globals;
	}
	public static class open extends TwoArgFunction
	{
		@Override
		public LuaValue call(LuaValue arg0, LuaValue arg1) {
			String path = arg0.toString();
			if(path.contains(".."))
			{
				return LuaValue.FALSE;
			}
			return lua_globals.get("io").get("open").call(LuaValue.valueOf(System.getProperty("user.home") + "/Documents/LuaComputers/" + path), arg1.tostring());
		}
		
	}
	
	public static class exists extends OneArgFunction
	{

		@Override
		public LuaValue call(LuaValue arg0) {
			String filename = arg0.toString();
			File file = new File(System.getProperty("user.home") + "/Documents/LuaComputers/" + filename);
			
			if(filename.contains(".."))
				return LuaValue.NIL;
			
			if(file.exists())
			{
				return LuaValue.TRUE;
			}else
			{
				return LuaValue.FALSE;
			}
			
		}
		
	}
	
	public static class makeDirectory extends OneArgFunction
	{

		@Override
		public LuaValue call(LuaValue arg0) {
			String fullPath = System.getProperty("user.home") + "/Documents/LuaComputers/" + arg0.toString();
			if(fullPath.contains(".."))
				return LuaValue.NIL;
			File dir = new File(fullPath);
			dir.mkdir();
			return null;
		}
		
	}
	
	public static class getRoot extends ZeroArgFunction
	{

		@Override
		public LuaValue call() {
			String rootPath = System.getProperty("user.home") + "/Documents/LuaComputers/";
			return LuaValue.valueOf(rootPath);
		}
		
	}
	
	public static class list extends OneArgFunction
	{

		@Override
		public LuaValue call(LuaValue arg0) {
			String fullPath = System.getProperty("user.home") + "/Documents/LuaComputers" + arg0.toString();
			
			if (fullPath.contains(".."))
			{
				Debugger.log("DEBUG: Tried to list files of " + fullPath);
				return LuaValue.NIL;
			}
			
			File dir = new File(fullPath);
			String[] list = dir.list();
			if(list == null)
			{
				Debugger.log("DEBUG: " + fullPath + " does not exist.");
				return LuaValue.NIL;
			}
			LuaTable table = new LuaTable();
			int counter = 1;
			for (int i = 0; i < list.length; i++)
			{
				table.insert(counter, LuaValue.valueOf(list[i]));
				counter++;
			}
			Debugger.log("FILESYSTEM LIST: " + fullPath + " TABLE_LENGTH: " + table.length());
			return table;
		}
	}
	
	public static class remove extends OneArgFunction
	{

		@Override
		public LuaValue call(LuaValue arg0) {
			String fullPath = System.getProperty("user.home") + "/Documents/LuaComputers/" + arg0.toString();
			
			if(fullPath.contains(".."))
				return LuaValue.NIL;
			File file = new File(fullPath);
			if(file.delete())
			{
				return LuaValue.TRUE;
			}else
			{
				return LuaValue.FALSE;
			}
		}
	}
}
