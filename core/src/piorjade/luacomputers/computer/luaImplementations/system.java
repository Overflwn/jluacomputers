/********************************************
* Copyright 2017 Patrick "Piorjade" Swierzy *
*********************************************/

package piorjade.luacomputers.computer.luaImplementations;

import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.TwoArgFunction;

import piorjade.luacomputers.Debugger;
import piorjade.luacomputers.StartClass;
import piorjade.luacomputers.Timer;

public class system 
{
	static StartClass mainClass;
	
	public static void setMain(StartClass main)
	{
		mainClass = main;
	}
	
	public static class pushEvent extends TwoArgFunction
	{

		@Override
		public LuaValue call(LuaValue arg0, LuaValue arg1) {
			synchronized(mainClass.events)
			{
				String eventName = arg0.toString();
				LuaValue[] values = {arg0, arg1.get(1), arg1.get(2), arg1.get(3), arg1.get(4)};
				mainClass.events.add(LuaValue.varargsOf(values));
				Debugger.log("EVENT: Queued custom event " + eventName);
				return null;
			}
		}
		
	}
	
	public static class setTimer extends OneArgFunction
	{

		@Override
		public LuaValue call(LuaValue arg0) {
			Timer timer = new Timer(arg0.tofloat());
			mainClass.timers.add(timer);
			return LuaValue.valueOf(timer.getID());
		}
		
	}
}
