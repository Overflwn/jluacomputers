/********************************************
* Copyright 2017 Patrick "Piorjade" Swierzy *
*********************************************/

package piorjade.luacomputers.computer.luaImplementations;
import piorjade.luacomputers.computer.Terminal;


import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.VarArgFunction;
import org.luaj.vm2.lib.ZeroArgFunction;

import com.badlogic.gdx.graphics.Color;

import piorjade.luacomputers.Debugger;
import piorjade.luacomputers.computer.CColor;

/**
 * This is just a "container" for static classes that get converted to a LuaTable.
 * They must be static because otherwise we would need to pass them "self", which isn't needed anyway.
 * @author patrick
 *
 */
public class gpu
{
	//The currently used Terminal instance
	static Terminal theTerminal;
	//The currently set color to use.
	static Color color = new Color(0.f, 0.f, 0.f, 1.f);
	//The currently set color to use as a LuaComputers value.
	static int cColor = 32768;

	/**
	 * Set the Terminal instance that the gpu API should use
	 * @param terminal
	 */
	public static void setTerminal(Terminal terminal)
	{
		theTerminal = terminal;
	}

	/**
	 * Return the Terminal instance that the gpu API uses
	 * @return
	 */
	public static Terminal getTerminal()
	{
		return theTerminal;
	}

	/**
	 * Clear the whole screen with the currently set color.
	 * Requires an integer as parameter. (2^x)
	 * @author patrick
	 *
	 */
	public static class clear extends ZeroArgFunction
	{

		@Override
		public LuaValue call() {
			// TODO Auto-generated method stub
			theTerminal.clear(color);
			return LuaValue.NIL;
		}

	}

	/**
	 * TestFunction
	 * @author patrick
	 *
	 */
	public static class print extends OneArgFunction
	{
		//TESTING
		@Override
		public LuaValue call(LuaValue arg0) {
			// TODO Auto-generated method stub
			Debugger.log(arg0.toString());
			return LuaValue.NIL;
		}
	}

	/**
	 * Sets the color that we use to set Pixels.
	 * Requires an integer as paremeter. (2^x)
	 * @author patrick
	 * @see CColor
	 */
	public static class setColor extends OneArgFunction
	{

		@Override
		public LuaValue call(LuaValue arg0) {
			// TODO Auto-generated method stub
			int newColor = arg0.toint();
			switch(newColor)
			{
			case CColor.WHITE:
				color = new Color(1.f, 1.f, 1.f, 1.f);
				cColor = newColor;
				break;
			case CColor.ORANGE:
				color = new Color(242.f/255.f, 178.f/255.f, 51.f/255.f, 1.f);
				cColor = newColor;
				break;
			case CColor.MAGENTA:
				color = new Color(229.f/255.f, 127.f/255.f, 216.f/255.f, 1.f);
				cColor = newColor;
				break;
			case CColor.LIGHTBLUE:
				color = new Color(153.f/255.f, 178.f/255.f, 242.f/255.f, 1.f);
				cColor = newColor;
				break;
			case CColor.YELLOW:
				color = new Color(222.f/255.f, 222.f/255.f, 108.f/255.f, 1.f);
				cColor = newColor;
				break;
			case CColor.LIME:
				color = new Color(127.f/255.f, 204.f/255.f, 25.f/255.f, 1.f);
				cColor = newColor;
				break;
			case CColor.PINK:
				color = new Color(242.f/255.f, 178.f/255.f, 204.f/255.f, 1.f);
				cColor = newColor;
				break;
			case CColor.GRAY:
				color = new Color(76.f/255.f, 76.f/255.f, 76.f/255.f, 1.f);
				cColor = newColor;
				break;
			case CColor.LIGHTGRAY:
				color = new Color(153.f/255.f, 153.f/255.f, 153.f/255.f, 1.f);
				cColor = newColor;
				break;
			case CColor.CYAN:
				color = new Color(76.f/255.f, 153.f/255.f, 178.f/255.f, 1.f);
				cColor = newColor;
				break;
			case CColor.PURPLE:
				color = new Color(178.f/255.f, 102.f/255.f, 229.f/255.f, 1.f);
				cColor = newColor;
				break;
			case CColor.BLUE:
				color = new Color(51.f/255.f, 102.f/255.f, 204.f/255.f, 1.f);
				cColor = newColor;
				break;
			case CColor.BROWN:
				color = new Color(127.f/255.f, 102.f/255.f, 76.f/255.f, 1.f);
				cColor = newColor;
				break;
			case CColor.GREEN:
				color = new Color(87.f/255.f, 166.f/255.f, 78.f/255.f, 1.f);
				cColor = newColor;
				break;
			case CColor.RED:
				color = new Color(204.f/255.f, 76.f/255.f, 76.f/255.f, 1.f);
				cColor = newColor;
				break;
			case CColor.BLACK:
				color = new Color(0.f, 0.f, 0.f, 1.f);
				cColor = newColor;
				break;
			case CColor.TRANSPARENT:
				color = new Color(0.f, 0.f, 0.f, 0.f);
				cColor = newColor;
			}
			return LuaValue.NIL;
		}
	}

	/**
	 * Returns the color that we currently use as an integer. (2^x)
	 * @author patrick
	 * @see CColor
	 */
	public static class getColor extends ZeroArgFunction
	{

		@Override
		public LuaValue call() {
			// TODO Auto-generated method stub
			return LuaValue.valueOf(cColor);
		}
	}

	/**
	 * Sets the pixel on the specified coordinate to the current color.
	 * Requires 2 integers (x, y) between 1 and the width / height as parameters.
	 * @author patrick
	 *
	 */
	public static class setPixel extends TwoArgFunction
	{

		@Override
		public LuaValue call(LuaValue arg0, LuaValue arg1) {
			// TODO Auto-generated method stub
			int x, y;
			x = arg0.toint();
			y = arg1.toint();
			theTerminal.setPixel(x, y, color);
			return LuaValue.NIL;
		}

	}

	/**
	 * Clears the specified line with the currently set color.
	 * Requires an integer (y) between 1 and the height as parameter.
	 * @author patrick
	 *
	 */
	public static class clearLine extends OneArgFunction
	{

		@Override
		public LuaValue call(LuaValue arg0) {
			// TODO Auto-generated method stub
			int y = arg0.toint();
			theTerminal.clearLine(y, color);
			return LuaValue.NIL;
		}

	}

	/**
	 * Clears the specified area with the currently set color.
	 * Requires 4 integers (x, y, width, height), all between 1 and the width/height as parameters.
	 * @author patrick
	 *
	 */
	public static class clearArea extends VarArgFunction
	{
		@Override
		public Varargs invoke(Varargs args)
		{
			theTerminal.clearArea(args.arg1().toint(), args.arg(2).toint(), args.arg(3).toint(), args.arg(4).toint(), color);
			return LuaValue.NIL;
		}

	}

	/**
	 * Shifts every pixel up by the specified amount.
	 * Requires an integer (amount) as parameter.
	 * @author patrick
	 *
	 */
	public static class scrollUp extends OneArgFunction
	{

		@Override
		public LuaValue call(LuaValue arg0) {
			// TODO Auto-generated method stub
			int count = arg0.toint();
			theTerminal.scrollUp(count);
			return LuaValue.NIL;
		}

	}

	/**
	 * Shifts every pixel down by the specified amount.
	 * Requires an integer (amount) as parameter.
	 * @author patrick
	 *
	 */
	public static class scrollDown extends OneArgFunction
	{

		@Override
		public LuaValue call(LuaValue arg0) {
			// TODO Auto-generated method stub
			int count = arg0.toint();
			theTerminal.scrollDown(count);
			return LuaValue.NIL;
		}

	}
}
