/********************************************
* Copyright 2017 Patrick "Piorjade" Swierzy *
*********************************************/

package piorjade.luacomputers.computer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import org.luaj.vm2.LuaValue;

import piorjade.luacomputers.Debugger;
import piorjade.luacomputers.StartClass;

/**
 * This class contains a UDP socket that does get initialized at the startup of the
 * program, however, it does not actually listen for messages until you "enable" that
 * adapter.
 * @author Piorjade
 */
public class NetworkAdapter extends Thread
{
	public boolean enabled = false;
	private DatagramSocket socket;
	private StartClass mainClass;
	private byte[] receiveBuffer;

	private Thread t;
	private String threadName;

	/**
	 * Create the adapter.
	 * @param mainClass
	 * @param port
	 * @param recv_buffer_size
	 * @param threadName
	 * @throws Exception
	 */
	public NetworkAdapter(StartClass mainClass, int port, int recv_buffer_size, String threadName) throws Exception
	{
		this.mainClass = mainClass;
		this.socket = new DatagramSocket(port);
		this.receiveBuffer = new byte[recv_buffer_size];
		this.threadName = threadName;
	}

	/**
	 * Run the adapter thread.
	 */
	public void run()
	{
		while (true)
		{
			//TODO: Not sure if I should use the synchronized keyword for enabled.
			if(mainClass.adapterEnabled)
			{
				DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
				try {
					socket.receive(receivePacket);
				} catch (IOException e) {
					System.err.format("Error with network adapter on thread %s", threadName);
					e.printStackTrace();
				}
				String message = new String(receivePacket.getData());
				Debugger.log("NETWORK: RECEIVED MESSAGE: " + message);
				LuaValue[] values = {LuaValue.valueOf("network_message"), LuaValue.valueOf(receivePacket.getAddress().toString()), LuaValue.valueOf(receivePacket.getPort()), LuaValue.valueOf(message)};
				mainClass.events.add(LuaValue.varargsOf(values));
			}
		}
	}

	/**
	 * Start the adapter.
	 */
	public void start()
	{
		Debugger.log("DEBUG: Starting Network adapter loop...");
		if(t == null)
		{
			t = new Thread(this, threadName);
			t.start();
		}
	}
}
