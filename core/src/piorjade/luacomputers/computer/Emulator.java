/********************************************
* Copyright 2017 Patrick "Piorjade" Swierzy *
*********************************************/

package piorjade.luacomputers.computer;
import org.luaj.vm2.*;
import org.luaj.vm2.lib.jse.*;

import piorjade.luacomputers.Debugger;
import piorjade.luacomputers.StartClass;
import piorjade.luacomputers.computer.luaImplementations.*;
public class Emulator 
{
	private Globals lua_globals;
	private LuaValue bios_code;
	private LuaThread bios_thread;
	private Varargs awaits;
	private CAudioDevice audio_device;
	
	private Terminal computer_gpuinal;
	
	/**
	 * Class that sets everything Lua related up and contains the bios.lua coroutine.
	 * @param terminal
	 * @param mainClass
	 * @param audio_device
	 */
	public Emulator(Terminal terminal, StartClass mainClass, CAudioDevice audio_device)
	{
		computer_gpuinal = terminal;
		this.audio_device = audio_device;
		audio.setAudioDevice(audio_device);
		gpu.setTerminal(computer_gpuinal);
		
		//See luadocs of LuaThread for information, I just copied this
		LuaThread.thread_orphan_check_interval = 500;
		
		//Get the standard lua globals (_G)
		lua_globals = JsePlatform.standardGlobals();
		
		//Convert the custom term implementation to an actual Lua table. 
		//(NOTE: the methods of the term implementation are static as non-static methods would need Lua to use term:blah instead of term.blah
		lua_globals.set("_gpu", new LuaTable());
		lua_globals.set("_filesystem", new LuaTable());
		lua_globals.set("_system", new LuaTable());
		lua_globals.set("_network", new LuaTable());
		lua_globals.set("_audio", new LuaTable());
		lua_globals.set("_http", new LuaTable());
		filesystem.setGlobals(lua_globals);
		system.setMain(mainClass);
		network.setMainClass(mainClass);
		http.setMainClass(mainClass);
		
		LuaValue term_print = CoerceJavaToLua.coerce(new gpu.print());
		lua_globals.get("_gpu").set("print", term_print);
		
		LuaValue term_clear = CoerceJavaToLua.coerce(new gpu.clear());
		lua_globals.get("_gpu").set("clear", term_clear);
		
		LuaValue term_setColor = CoerceJavaToLua.coerce(new gpu.setColor());
		lua_globals.get("_gpu").set("setColor", term_setColor);
		
		LuaValue term_getColor = CoerceJavaToLua.coerce(new gpu.getColor());
		lua_globals.get("_gpu").set("getColor", term_getColor);
		
		LuaValue term_setPixel = CoerceJavaToLua.coerce(new gpu.setPixel());
		lua_globals.get("_gpu").set("setPixel", term_setPixel);
		
		LuaValue term_clearLine = CoerceJavaToLua.coerce(new gpu.clearLine());
		lua_globals.get("_gpu").set("clearLine", term_clearLine);
		
		LuaValue term_clearArea = CoerceJavaToLua.coerce(new gpu.clearArea());
		lua_globals.get("_gpu").set("clearArea", term_clearArea);
		
		LuaValue term_scrollUp = CoerceJavaToLua.coerce(new gpu.scrollUp());
		lua_globals.get("_gpu").set("scrollUp", term_scrollUp);
		
		LuaValue term_scrollDown = CoerceJavaToLua.coerce(new gpu.scrollDown());
		lua_globals.get("_gpu").set("scrollDown", term_scrollDown);
		
		
		LuaValue fs_open = CoerceJavaToLua.coerce(new filesystem.open());
		lua_globals.get("_filesystem").set("open", fs_open);
		
		LuaValue fs_exists = CoerceJavaToLua.coerce(new filesystem.exists());
		lua_globals.get("_filesystem").set("exists", fs_exists);
		
		LuaValue fs_makeDirectory = CoerceJavaToLua.coerce(new filesystem.makeDirectory());
		lua_globals.get("_filesystem").set("makeDirectory", fs_makeDirectory);
		
		LuaValue fs_list = CoerceJavaToLua.coerce(new filesystem.list());
		lua_globals.get("_filesystem").set("list", fs_list);
		
		LuaValue fs_remove = CoerceJavaToLua.coerce(new filesystem.remove());
		lua_globals.get("_filesystem").set("remove", fs_remove);
		
		LuaValue fs_getRoot = CoerceJavaToLua.coerce(new filesystem.getRoot());
		lua_globals.get("_filesystem").set("getRoot", fs_getRoot);
		
		
		LuaValue sys_pushEvent = CoerceJavaToLua.coerce(new system.pushEvent());
		lua_globals.get("_system").set("pushEvent", sys_pushEvent);
		
		LuaValue sys_setTimer = CoerceJavaToLua.coerce(new system.setTimer());
		lua_globals.get("_system").set("setTimer", sys_setTimer);
		
		
		LuaValue network_toggleAdapter = CoerceJavaToLua.coerce(new network.toggleAdapter());
		lua_globals.get("_network").set("toggleAdapter",network_toggleAdapter);
		
		LuaValue network_enabled = CoerceJavaToLua.coerce(new network.enabled());
		lua_globals.get("_network").set("enabled", network_enabled);
		
		LuaValue network_send = CoerceJavaToLua.coerce(new network.send());
		lua_globals.get("_network").set("send", network_send);
		
		
		LuaValue http_toggleAdapter = CoerceJavaToLua.coerce(new http.toggleAdapter());
		lua_globals.get("_http").set("toggleAdapter", http_toggleAdapter);
		
		LuaValue http_enabled = CoerceJavaToLua.coerce(new http.enabled());
		lua_globals.get("_http").set("enabled", http_enabled);
		
		LuaValue http_send = CoerceJavaToLua.coerce(new http.send());
		lua_globals.get("_http").set("send", http_send);
		
		
		LuaValue audio_play = CoerceJavaToLua.coerce(new audio.playSound());
		lua_globals.get("_audio").set("playSound", audio_play);
		
		LuaValue audio_load = CoerceJavaToLua.coerce(new audio.loadSound());
		lua_globals.get("_audio").set("loadSound", audio_load);
		//Security fixes
		
		//Remove the luajava library from _G
		lua_globals.set("luajava", LuaValue.NIL);
		
		//Load the bios.lua code and create a coroutine / LuaThread out of it. (TODO: Not tested yet, not sure if LuaThread is the right thing)
		bios_code = lua_globals.loadfile("bios.lua");
		
		bios_thread = new LuaThread(lua_globals, bios_code);
	}
	
	/**
	 * Resumes the bios.lua coroutine with the given event.
	 * @param args
	 * @return
	 */
	public boolean resume(Varargs args)
	{
		if(awaits == null || args.arg1() == awaits.arg1() || awaits.isnil(1) || (awaits.isstring(1) && awaits.arg1().length() == 0))
		{
			awaits = bios_thread.resume(args);
			if(!awaits.arg1().toboolean())
			{
				//An error occured, print it.
				System.err.println("BIOS ERROR: " + awaits.arg(2).toString());
				return false;
			} else
			{
				LuaValue[] values = {awaits.arg(2)};
				awaits = LuaValue.varargsOf(values);
				return true;
			}
		} else
		{
			Debugger.log("DEBUG: Emulator expected '" + awaits.arg1().toString() + "' but got '" + args.arg1().toString() + "', not resuming.");
			return true;
		}
	}
}
