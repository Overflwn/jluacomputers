/********************************************
* Copyright 2017 Patrick "Piorjade" Swierzy *
*********************************************/

package piorjade.luacomputers.computer;

import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;

import com.badlogic.gdx.Gdx;

import piorjade.luacomputers.Debugger;
import piorjade.luacomputers.StartClass;

public class EmulatorTask extends Thread
{
	private Thread t;
	private Emulator emulator;
	private String threadName;
	private StartClass mainThread;
	
	/**
	 * Create a thread for the emulator.
	 * @param emulator
	 * @param threadName
	 * @param mainThread
	 */
	public EmulatorTask(Emulator emulator, String threadName, StartClass mainThread)
	{
		this.emulator = emulator;
		this.threadName = threadName;
		this.mainThread = mainThread;
	}
	
	/**
	 * Run the emulator thread.
	 */
	public void run()
	{
		while(true)
		{
			LuaValue[] valuess = {LuaValue.NIL};
			Varargs oldestEvent = LuaValue.varargsOf(valuess);
			if(mainThread.events.size() > 0)
			{
				oldestEvent = mainThread.events.poll();
			} else
			{
				LuaValue[] values = {LuaValue.NIL};
				oldestEvent = LuaValue.varargsOf(values);
			}
			
			
			if (oldestEvent.arg1().toString().equals("network_message"))
			{
				Debugger.log("NETWORK: Received network message..");
			}
				
			boolean success = emulator.resume(oldestEvent);
			if (!success)
			{
				System.err.println("ERROR: BIOS coroutine has stopped, shutting down...");
				Gdx.app.exit();
			}
		}
	}
	
	public void Start()
	{
		Debugger.log("DEBUG: Starting Emulator loop...");
		if(t == null)
		{
			t = new Thread(this, threadName);
			t.start();
		}
	}
}
