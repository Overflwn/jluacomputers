/********************************************
 * Copyright 2017 Patrick "Piorjade" Swierzy *
 *********************************************/

package piorjade.luacomputers.computer;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

class Pixel
{
	public Color color;

	public Pixel(Color color)
	{
		this.color = color;
	}
}

public class Terminal
{

	//TODO: Commented direct shapeRenderer calls, as they weren't useful in the first place (the complete terminal got drawn directly after that anyway)
	//		and it didn't work after I made the emulator a seperate thread anyway
	private int terminal_width, terminal_height;
	private int window_width, window_height;
	private int pixel_width, pixel_height;
	private ShapeRenderer shapeRenderer;
	private Pixel[][] pixels;

	/**
	 * Creates a terminal object width the given width and height.
	 * NOTE: The terminal uses single pixels, not these pseudo-pixels from ComputerCraft,
	 * which have a char on each one of them.
	 */
	public Terminal(int width, int height, int window_width, int window_height)
	{

		terminal_width = width;
		terminal_height = height;
		pixels = new Pixel[width][height];
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				pixels[x][y] = new Pixel(new Color(0.f, 0.f, 0.f, 1.f));
			}
		}
		this.window_width = window_width;
		this.window_height = window_height;
		this.pixel_width = Math.round(window_width / terminal_width);
		this.pixel_height = Math.round(window_height/terminal_height);


	}

	/**
	 * Draws every pixel on the given shapeRenderer.
	 */
	public void drawTerminal()
	{

		for(int x = 0; x < terminal_width; x++)
		{
			for(int y = 0; y < terminal_height; y++)
			{
				shapeRenderer.setColor(pixels[x][y].color);

				shapeRenderer.rect(pixel_width*(x+1)-pixel_width, window_height - (pixel_height*(y+1)-pixel_height) - pixel_height, pixel_width, pixel_height);
			}
		}
	}

	/**
	 * Sets the shapeRenderer that gets used.
	 * @param shapeRenderer
	 */
	public void setRenderer(ShapeRenderer shapeRenderer)
	{
		this.shapeRenderer = shapeRenderer;
	}

	/**
	 * Sets a specific pixel on the terminal to the
	 * given color.
	 * NOTE: The coordinates HAVE TO be inside the terminal dimensions.
	 * @param x
	 * @param y
	 * @param color
	 * @return
	 */
	public boolean setPixel(int x, int y, Color color)
	{

		if (x > terminal_width || x < 1)
			return false;
		if (y > terminal_height || y < 1)
			return false;
		pixels[x-1][y-1] = new Pixel(color);
		return true;
	}

	/**
	 * Returns the color of that pixel.
	 * NOTE: The coordinates HAVE TO be inside the terminal dimensions.
	 * @param x
	 * @param y
	 * @return
	 */
	public Color getPixel(int x, int y)
	{
		if (x > terminal_width || x < 1)
			return new Color(0.f, 0.f, 0.f, 0.f);
		if (y > terminal_height || y < 1)
			return new Color(0.f, 0.f, 0.f, 0.f);
		return pixels[x-1][y-1].color;
	}

	/**
	 * Clears the whole terminal with the given color.
	 * @param color
	 */
	public void clear(Color color)
	{
		for(int xc = 0; xc < terminal_width; xc++)
		{
			for(int yc = 0; yc < terminal_height; yc++)
			{
				pixels[xc][yc] = new Pixel(color);
			}
		}
	}

	/**
	 * Clears a set area on the terminal with the given color.
	 * NOTE: The coordinates and dimension have to be inside the terminal dimensions.
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param color
	 */
	public void clearArea(int x, int y, int width, int height, Color color)
	{
		if (x > terminal_width || x < 1)
			return;
		if (y > terminal_height || y < 1)
			return;
		if (width > terminal_width || width < 1)
			return;
		if (height > terminal_height || height < 1)
			return;

		x = x-1;
		y = y-1;

		for(int xc = 0; xc < width; xc++)
		{
			for(int yc = 0; yc < height; yc++)
			{
				pixels[x+xc][y+yc] = new Pixel(color);
			}
		}
	}

	/**
	 * Clears a whole horizontal line on the y-coordinate on the terminal with the given color.
	 * NOTE: The coordinate HAS TO be inside the terminal dimensions.
	 * @param y
	 * @param color
	 */
	public void clearLine(int y, Color color)
	{
		if (y > terminal_height || y < 1)
			return;
		clearArea(1, y, terminal_width, 1, color);
	}

	/**
	 * Shifts up every line up by the given amount.
	 * NOTE: New lines appear in black color at the bottom.
	 * @param count
	 */
	public void scrollUp(int count)
	{
		if (count > terminal_height)
			count = terminal_height;
		if (count == terminal_height)
		{
			clearArea(1, 1, terminal_width, terminal_height, new Color(0.f, 0.f, 0.f, 1.f));
			return;
		}
		if (count < 1)
			return;

		for (int i = 1; i <= count; i++)
		{
			for (int x = 0; x < terminal_width; x++)
			{
				for (int y = 0; y < terminal_height; y++)
				{
					if (y+1 <= terminal_height-1)
					{
						pixels[x][y] = pixels[x][y+1];
					} else
					{
						pixels[x][y] = new Pixel(new Color(0.f, 0.f, 0.f, 1.f));
					}
				}
			}
		}

	}

	/**
	 * Shifts up every line down by the given amount.
	 * NOTE: New lines appear in black color at the top.
	 * @param count
	 */
	public void scrollDown(int count)
	{
		if (count > terminal_height)
			count = terminal_height;
		if (count == terminal_height)
		{
			clearArea(1, 1, terminal_width, terminal_height, new Color(0.f, 0.f, 0.f, 1.f));
			return;
		}
		if (count < 1)
			return;

		for (int i = 1; i <= count; i++)
		{
			for (int x = 0; x < terminal_width; x++)
			{
				for (int y = 0; y < terminal_height; y++)
				{
					if (terminal_height-1-y-1 >= 0)
					{
						pixels[x][terminal_height-1-y] = pixels[x][terminal_height-1-y-1];
					} else
					{
						pixels[x][terminal_height-1-y] = new Pixel(new Color(0.f, 0.f, 0.f, 1.f));
					}
				}
			}
		}

	}

}
