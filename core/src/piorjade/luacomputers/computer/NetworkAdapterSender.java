/********************************************
* Copyright 2017 Patrick "Piorjade" Swierzy *
*********************************************/

package piorjade.luacomputers.computer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

import piorjade.luacomputers.Debugger;
import piorjade.luacomputers.NetworkMessage;
import piorjade.luacomputers.StartClass;

/**
 * This class contains a UDP socket that does get initialized at the startup of the
 * program, however, it does not actually listen for messages until you "enable" that
 * adapter.
 * @author patrick
 *
 */
public class NetworkAdapterSender extends Thread {
	
	public boolean enabled = false;
	private DatagramSocket socket;
	private StartClass mainClass;
	private byte[] sendBuffer;
	private int port;
	
	private Thread t;
	private String threadName;
	
	/**
	 * Create the adapter.
	 * @param mainClass
	 * @param port
	 * @param recv_buffer_size
	 * @param send_buffer_size
	 * @param threadName
	 * @throws Exception
	 */
	public NetworkAdapterSender(StartClass mainClass, int port, int send_buffer_size, String threadName) throws Exception
	{
		this.mainClass = mainClass;
		this.port = port;
		this.socket = new DatagramSocket();
		this.sendBuffer = new byte[send_buffer_size];
		this.threadName = threadName;
	}
	
	/**
	 * Run the adapter thread.
	 */
	public void run()
	{
		while (true)
		{
			//TODO: Check whether or not the synchronization of adapterEnabled actually works.
			if(mainClass.adapterEnabled)
			{
				if(mainClass.messageQueue.size() > 0)
				{

					for (int i = 0; i < mainClass.messageQueue.size(); i++)
					{
						NetworkMessage msg = mainClass.messageQueue.poll();
						sendBuffer = msg.getMessage().getBytes();
						try {
							InetAddress ipAddr = InetAddress.getByName(msg.getAddress());
							DatagramPacket sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length, ipAddr, port);
							socket.send(sendPacket);
						} catch (UnknownHostException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
	
	/**
	 * Start the adapter.
	 */
	public void start()
	{
		Debugger.log("DEBUG: Starting Network Sender adapter loop...");
		if(t == null)
		{
			t = new Thread(this, threadName);
			t.start();
		}
	}
}
