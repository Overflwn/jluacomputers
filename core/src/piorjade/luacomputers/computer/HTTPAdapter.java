/********************************************
* Copyright 2017 Patrick "Piorjade" Swierzy *
*********************************************/

package piorjade.luacomputers.computer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Iterator;


import org.luaj.vm2.LuaValue;

import piorjade.luacomputers.Debugger;
import piorjade.luacomputers.HTTPRequest;
import piorjade.luacomputers.StartClass;

/**
* This class allows us to create HTTP requests (POST and GET) and receive data
* TODO: Try out whether or not to make this a thread, depending on whether or not
*       this actually halts the current thread or not when requesting data.
* TODO: Think of proper code & do research as I don't have a f*cking clue of what to do.
*       (never did ANYTHING with http stuff)
* @author Piorjade
*/
public class HTTPAdapter extends Thread
{
  /**
  * Constructor
  * TODO: Add information
  * @author Piorjade
  */
  StartClass mainClass;
  
  private Thread t;
  private String threadName;
  
  public HTTPAdapter(StartClass mainClass, String threadName)
  {
    this.mainClass = mainClass;
    this.threadName = threadName;
  }
  
  public void run()
	{
		while (true)
		{
			//TODO: Check whether or not the synchronization of adapterEnabled actually works.
			if(mainClass.httpEnabled)
			{
				//synchronized(mainClass.requestQueue)
				//{
					if(mainClass.requestQueue.size() > 0)
					{
						
						//TODO: Again, not sure how the iterator works so I am doing it manually
						/*for(Iterator<HTTPRequest> iterator = mainClass.requestQueue.iterator(); iterator.hasNext();)
						{
							HTTPRequest request = iterator.next();
							String type = request.getType();
							String urlLink = request.getUrlLink();
							Debugger.log("HTTP SEND: " + urlLink + " TYPE: "+type);
							long id = request.getID();
							try {
								URL url = new URL(urlLink);
								HttpURLConnection conn;
								conn = (HttpURLConnection)url.openConnection();
								conn.setRequestMethod(type);
								
								BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
								StringBuffer sb = new StringBuffer();
								String line;
								
								while((line = in.readLine()) != null)
								{
									sb.append(line);
								}
								
								in.close();
								Debugger.log("HTTP FINISHED : " + sb.toString());
								synchronized(mainClass.events)
								{
									//Pack the response to a LuaComputers event and put it into the queue
									LuaValue[] values = {LuaValue.valueOf("http_response"), LuaValue.valueOf(id), LuaValue.valueOf(sb.toString())};
									mainClass.events.add(LuaValue.varargsOf(values));
								}
								iterator.remove();
							} catch (MalformedURLException e) {
								System.err.println("Invalid URL given!: " + urlLink);
								e.printStackTrace();
							} catch (ProtocolException e) {
								System.err.println("Invalid Request type given!");
								e.printStackTrace();
							} catch (IOException e) {
								System.err.println("Error while opening the connection!");
								e.printStackTrace();
							}
						}*/
						
						for(int i = 0; i < mainClass.requestQueue.size(); i++)
						{
							HTTPRequest request = mainClass.requestQueue.poll();
							String type = request.getType();
							String urlLink = request.getUrlLink();
							Debugger.log("HTTP SEND: " + urlLink + " TYPE: "+type);
							long id = request.getID();
							try {
								URL url = new URL(urlLink);
								HttpURLConnection conn;
								conn = (HttpURLConnection)url.openConnection();
								conn.setRequestMethod(type);
								
								BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
								StringBuffer sb = new StringBuffer();
								String line;
								
								while((line = in.readLine()) != null)
								{
									sb.append(line);
								}
								
								in.close();
								Debugger.log("HTTP FINISHED : " + sb.toString());
								synchronized(mainClass.events)
								{
									//Pack the response to a LuaComputers event and put it into the queue
									LuaValue[] values = {LuaValue.valueOf("http_response"), LuaValue.valueOf(id), LuaValue.valueOf(sb.toString())};
									mainClass.events.add(LuaValue.varargsOf(values));
								}
							} catch (MalformedURLException e) {
								System.err.println("Invalid URL given!: " + urlLink);
								e.printStackTrace();
							} catch (ProtocolException e) {
								System.err.println("Invalid Request type given!");
								e.printStackTrace();
							} catch (IOException e) {
								System.err.println("Error while opening the connection!");
								e.printStackTrace();
							}
						}
						
					}
				//}
			}
		}
	}
	
	public void start()
	{
		Debugger.log("DEBUG: Starting HTTP adapter loop...");
		if(t == null)
		{
			t = new Thread(this, threadName);
			t.start();
		}
	}

}
