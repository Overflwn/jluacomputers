/********************************************
* Copyright 2017 Patrick "Piorjade" Swierzy *
*********************************************/

package piorjade.luacomputers.computer;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;

import piorjade.luacomputers.Debugger;

//TODO: Looping sounds makes it cut when it gets restarted

/**
 * Internal class creates a thread that terminates the sound instance after a given time.
 * @author patrick
 *
 */
class SoundTimer extends TimerTask
{
	private long soundID;
	private Sound sound;
	
	/**
	 * Create the killtimer.
	 * @param sound
	 * @param soundID
	 */
	public SoundTimer(Sound sound, long soundID)
	{
		this.sound = sound;
		this.soundID = soundID;
	}
	
	/**
	 * Run the killtimer.
	 */
	public void run()
	{
		sound.stop(soundID);
	}
}

/**
 * Internal class acting as a container for sounds.
 * @author patrick
 *
 */
class CSound
{
	private Sound theSound;
	private String name;
	
	/**
	 * Create a container for a sound.
	 * @param name
	 * @param sound
	 */
	public CSound(String name, Sound sound)
	{
		this.name = name;
		theSound = sound;
	}
	
	/**
	 * Return the actual sound.
	 * @return
	 */
	public Sound getSound()
	{
		return theSound;
	}
	
	/**
	 * Return the name of the container.
	 * @return
	 */
	public String getName()
	{
		return name;
	}
}

public class CAudioDevice
{
	private ArrayList<CSound> soundList = new ArrayList<CSound>();
	private Timer timer;
	
	/**
	 * Load included sounds.
	 */
	void addInternalSounds()
	{
		//In case somebody removed the file
		if(Gdx.files.internal("beep.ogg").exists())
		{
			loadSound("beep", "beep.ogg");
		}
	}
	
	/**
	 * Create an audio device.
	 */
	public CAudioDevice()
	{
		addInternalSounds();
		this.timer = new Timer();
	}
	
	/**
	 * Load a sound by its path and set the containers name.
	 * @param name
	 * @param path
	 * @return
	 */
	public boolean loadSound(String name, String path)
	{
		/**
		 * Loads a sound with the given path and sets it's name.
		 */
		Debugger.log("DEBUG: Trying to load Sound(" + name + ").");
		boolean found = false;
		for(CSound sound: soundList)
		{
			if(sound.getName().equals(name))
			{
				found = true;
				break;
			}
		}
		if (!found)
		{
			FileHandle file = Gdx.files.external("/Documents/LuaComputers/" + path);
			if(file.exists())
			{
				CSound newSound = new CSound(name, Gdx.audio.newSound(file));
				soundList.add(newSound);
				
				Debugger.log("DEBUG: Sound (" + name + ") loaded.");
				return true;
			}else if(Gdx.files.internal(path).exists())
			{
				soundList.add(new CSound(name, Gdx.audio.newSound(Gdx.files.internal(path))));
				Debugger.log("DEBUG: Sound (" + name + ") loaded.");
				return true;
			}
			return false;
		}
		Debugger.log("DEBUG: Sound (" + name + ") already loaded.");
		return true;
	}
	
	/**
	 * Unload the sound by its container name.
	 * @param name
	 */
	public void unloadSound(String name)
	{
		
		Debugger.log("DEBUG: Trying to unload Sound(" + name + ").");
		CSound theSound = null;
		for(CSound sound: soundList)
		{
			if(sound.getName().equals(name))
			{
				theSound = sound;
				break;
			}
		}
		if(theSound != null)
		{
			soundList.remove(theSound);
			Debugger.log("DEBUG: Sound (" + name + ") unloaded.");
		}else
		{
			Debugger.log("DEBUG: Sound (" + name + ") not loaded.");
		}
	}
	
	/**
	 * Plays a loaded sound (selected by it's given name) and sets it's pitch, pan and the
	 * amount of milliseconds it should be played.
	 * @param name
	 * @param pitch
	 * @param pan
	 * @param amount
	 * @return
	 */
	public boolean playSound(String name, float pitch, float pan, long amount)
	{
		Debugger.log("DEBUG: Trying to play Sound(" + name + ").");
		boolean found = false;
		CSound theSound = null;
		for(CSound sound: soundList)
		{
			if(sound.getName().equals(name))
			{
				found = true;
				theSound = sound;
				break;
			}
		}
		if(found)
		{
			long id = theSound.getSound().play(1.0f, pitch, pan);
			theSound.getSound().setLooping(id, true);
			//Stop the sound after a certain time
			timer.schedule(new SoundTimer(theSound.getSound(), id), amount);
			Debugger.log("DEBUG: Playing Sound(" + name + ").");
			return true;
		}
		Debugger.log("DEBUG: Sound (" + name + ") not loaded.");
		return false;
	}
	
	/**
	 * Plays a loaded sound (selected by it's given name) and sets it's pitch and pan.
	 * @param name
	 * @param pitch
	 * @param pan
	 * @return
	 */
	public boolean playSound(String name, float pitch, float pan)
	{
		Debugger.log("DEBUG: Trying to play Sound(" + name + ").");
		boolean found = false;
		CSound theSound = null;
		for(CSound sound: soundList)
		{
			if(sound.getName().equals(name))
			{
				found = true;
				theSound = sound;
				break;
			}
		}
		if(found)
		{
			long id = theSound.getSound().play(1.0f, pitch, pan);
			if(id == -1)
			{
				Debugger.log("DEBUG: Failed to play Sound(" + name + ").");
				return false;
			}
			Debugger.log("DEBUG: Playing Sound(" + name + ").");
			return true;
		}
		Debugger.log("DEBUG: Sound (" + name + ") not loaded.");
		return false;
	}
	
	/**
	 * Disposes every loaded sound.
	 */
	public void dispose()
	{
		for(CSound sound: soundList)
		{
			Debugger.log("DEBUG: Disposing Sound(" + sound.getName() + ").");
			sound.getSound().dispose();
		}
	}
}
