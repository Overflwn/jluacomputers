/********************************************
* Copyright 2017 Patrick "Piorjade" Swierzy *
*********************************************/

package piorjade.luacomputers.computer;

/**
 * Static class containing every available color.
 * @author patrick
 *
 */
public class CColor 
{
	public static final int WHITE = 1;
	public static final int ORANGE = 2;
	public static final int MAGENTA = 4;
	public static final int LIGHTBLUE = 8;
	public static final int YELLOW = 16;
	public static final int LIME = 32;
	public static final int PINK = 64;
	public static final int GRAY = 128;
	public static final int LIGHTGRAY = 256;
	public static final int CYAN = 512;
	public static final int PURPLE = 1024;
	public static final int BLUE = 2048;
	public static final int BROWN = 4069;
	public static final int GREEN = 8192;
	public static final int RED = 16384;
	public static final int BLACK = 32768;
	public static final int TRANSPARENT = 65536;
}
