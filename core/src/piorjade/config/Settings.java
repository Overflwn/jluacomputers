/********************************************
* Copyright 2017 Patrick "Piorjade" Swierzy *
*********************************************/

package piorjade.config;

public class Settings 
{
	public static final int RES_X = 320;
	public static final int RES_Y = 200;
	public static final int NETWORK_PORT = 6969;
	public static final int RECEIVE_BUFFER_SIZE = 1024;
	public static final int SEND_BUFFER_SIZE = 1024;
}
