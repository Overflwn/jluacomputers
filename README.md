# **WARNING: This version is DEPRECATED!**
**I ported LuaComputers to C/C++ (with SFML) for performance and educational reasons.**

**Link to the GitHub page (I will switch back to GitLab some time though): [Here](https://github.com/Piorjade/LUAComputers)**

# LuaComputers - Java Edition

## Description

Open up a retro-like virtual computer and start programming games and other software in Lua!

## Info

### Libraries used

- libGDX (everything graphics related and audio)
- LuaJ (lua wrapper)
- Standard Java Net library for networking (HTTP and sockets)