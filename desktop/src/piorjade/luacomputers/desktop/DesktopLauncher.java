package piorjade.luacomputers.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import piorjade.luacomputers.StartClass;
import piorjade.config.WindowSettings;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.resizable = false;
		config.width = WindowSettings.WINDOW_WIDTH;
		config.height = WindowSettings.WINDOW_HEIGHT;
		config.title = WindowSettings.WINDOW_TITLE;
		config.vSyncEnabled = false;
		config.foregroundFPS = 0;
		config.backgroundFPS = 0;
		config.forceExit = true;
		new LwjglApplication(new StartClass(WindowSettings.WINDOW_WIDTH, WindowSettings.WINDOW_HEIGHT), config);
	}
}
