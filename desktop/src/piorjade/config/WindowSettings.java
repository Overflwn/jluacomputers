package piorjade.config;

public class WindowSettings 
{
	public static final int WINDOW_WIDTH = 1600;
	public static final int WINDOW_HEIGHT = 1000;
	public static final String WINDOW_TITLE = "LuaComputers";
}
